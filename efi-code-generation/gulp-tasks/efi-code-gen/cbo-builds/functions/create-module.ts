!(function(){



module.exports = (folders) => {

    const service = getServiceR(folders.api, '', '');

    return (
        service._imports +
        `
    import { NgModule } from '@angular/core';
    import { HttpClientModule } from '@angular/common/http';
    @NgModule({
      imports: [
        HttpClientModule,
      ],
      providers: [
          ${service.providers}
      ]
    })
    export class ChameleonApiModule { }
    `
    );
}

function getServiceR(folder, _imports, providers) {

    if (folder.folder) {
        return getServiceR(folder.folder, _imports, providers);
    }
    let counter = 0;
    for (const key in folder) {
        const item = folder[key];
        if(!item.paths[0]) {
            continue;
        }            
        const arr = item.paths[0].split('/');
        let pa;
        do {
            pa = arr.pop();
        } while (pa.indexOf("{") !== -1);
        _imports += `import { ${item.name}ServiceApi } from '.${arr.join('/')}/${item.name}ServiceApi';\n`;
        providers+= `${item.name}ServiceApi, `

        if(counter !== 0 && counter % 3 === 0) {
            providers+='\n      ';
        }
        counter++;
    }
    return {_imports, providers};
}


})();
