import { log } from "util";


(function () {

    let counter = 0; /// 'c_' + (++counter) + '_' + 

    const getType = require('./types');
    const getDefValue = require('./def-values');

    const removeCharsR = require('./chars');
    module.exports = function addDef(name, json) {

        if(name === 'Newborn') {
            name = 'NewBorn2'
        }
        
        let properties = '';
        let dependencies = '';
        for (const key in json.properties) {
            const variable = key.split('');
            // variable[0] = variable[0][0].toLowerCase();
            
            const type = getType(json.properties[key]);
            const res = getDefValue(type);
            const val = res.type || res;
            if(res.dep && dependencies.indexOf(' { ' + res.dep + ' } ') === -1 && 
            res.dep !== 'any' && res.dep != name) {
                dependencies += (`
                    import { ${res.dep} } from './${res.dep}';
                `);
            }
            const _prop: string = 'public ' + (variable.join('')) + ': ' + type + val;

            properties += '\n    ' + _prop + ';';
        }

        if (name.indexOf('Culture=neutral') !== -1) {
            return (
                `\n\n\n${dependencies}
                export class UnknowInterfaceDetected${counter++} {
                 //// ${name}
                    ${properties}
                } \n\n\n`
            );
        }

        counter++;

        if(name === "GetIVFCarePlanQuery") {
            name = "GetIVFCarePlanQuery1";
        }
        if(name === "GetIVFCarePlanResponse") {
            name = "GetIVFCarePlanResponse1";
        }

        return (
            `/* tslint:disable */\n${dependencies}\n\nexport class ` + removeCharsR(name) + ' {' +
             properties + `
             \n}\n\n\n`
        );
    };

})();

