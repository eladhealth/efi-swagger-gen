(function() {
    const removeCharsR = require("./chars");
  
    module.exports = function getDefValue(type) {
      switch (type) {
        case "integer":
        case "number":
          return " = 0";
  
        case "array":
          return " = []";
  
        case "string":
          return " = ''";
        case "type":
          return "";
  
          case "boolean":
          return " = false";
  
          case "any":
          return " = {}";
  
        default:
          if(type.indexOf('Newborn') !== -1) {
            type.replace('Newborn', 'NewBorn2');
          }
          var res1 = type.substr(type.indexOf('<') + 1)
          var res = res1.substr(0, res1.indexOf('>'));
          return {
            dep: res || type,
            type: " = new " + type + "()"
          };
      }
  
    };
  })();
  