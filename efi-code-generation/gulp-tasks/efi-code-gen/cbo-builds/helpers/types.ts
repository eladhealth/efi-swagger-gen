

(function () {


    const removeCharsR = require('./chars');

    module.exports = function getType(property) {

        switch (property.type) {

            case 'integer':
                return 'number';


            case 'array':
                const ref = property.items.$ref;
                if (!ref) {
                    return getType(property.items);
                }
                if (ref.indexOf('#/definitions/0') !== -1) {
                    return 'Array<any>';
                }
                const val = ref.substr(ref.lastIndexOf('/') + 1);
                return 'Array<' + removeCharsR(val) + '>';


            case 'object':
                const ref2 = property.items || property.additionalProperties;
                if (ref2) {
                    console.log(ref2);
                    return getType(ref2);
                }
                return 'any';



            case 'string':
            case 'number':
            case 'type':
            case 'boolean':
                return property.type;

        }

        if (property.$ref) {
            return removeCharsR(property.$ref.substr(property.$ref.lastIndexOf('/') + 1));
        }

        if (property.schema) {
            if (property.schema.type) {
                return getType(property.schema);
            }
            if (!property.schema.$ref) {
                console.log(property.schema);
                return getType(property.schema);
            }
            return removeCharsR(property.schema.$ref.substr(property.schema.$ref.lastIndexOf('/') + 1));
        }

        if (Object.keys(property).length === 0) {
            return 'any';
        }
        console.log('UNKNOW VALUE: ' + JSON.stringify(property));
        return 'any /*(UNKNOW VALUE) property: ' + JSON.stringify(property) + ' */' ;

    };

})();

