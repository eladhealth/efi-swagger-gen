
(function () {

    const getType = require('./types');

    module.exports = (parameters) => {
        const expect = [];
        for (const key in parameters) {
            if (parameters[key].schema) {
                if (!parameters[key].schema.$ref) {
                    expect.push(getType(parameters[key].schema));
                    continue;
                }
                expect.push(parameters[key].schema.$ref.split('/').pop());
            }
        }
        if (!expect.length) {
            expect.push('void');
        }
        return expect.join(' | ');
    };

})();

