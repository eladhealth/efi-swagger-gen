

(function() {

    const gulp = require("gulp");
    const file = require("gulp-file");
    const configuration = require("./../helpers/configurations");


    module.exports = function (fileName: string, content: string, destination: string = '') {
        gulp
        .src("Must-No-Have-A-Real-Path/**.js")
        .pipe(file(fileName, content))
        .pipe(
            gulp.dest(configuration.path + destination)
        );

    } 

})();


