
(function () {

    const gulp = require("gulp");
    const file = require('gulp-file');
    const configurations = require('./helpers/configurations');
    // const addDef = require('./handlers/add-def');
    const addDef = require('./helpers/add-def');

    function interfacesCreator(json, done) {

        console.log('Creating interfaces....................');
        // #region
        const arr = [];
        let counter = 0;
        console.log('Creating interfaces from swager');
        for (const key in json.definitions) {
            counter++;
            arr.push(addDef(key, json.definitions[key]));
        }
        // console.log(keys.join(', '));
        console.log('.');
        console.log('.');
        console.log(counter + ' interface created successfully');
        console.log('===================================================');
        console.log('===================================================');
        console.log(' ');
        console.log(' ');
        // #endregion

        const models = [];
        arr.forEach(output => {

            const part1 = output.substr(output.indexOf('class') + 6);
            const name = part1.substr(0, part1.indexOf('{') - 1);
            models.push(name);
            gulp.src('Must-No-Have-A-Real-Path/**.js')
                .pipe(file(name + '.ts', output))
                .pipe(gulp.dest(configurations.path + '/models'));

        });

        // let output = '';
        // let index = 0;
        // models.forEach(model => {
        //     index++;
        //     var v = model;
        //     v[0] = v[0].toLowerCase();
        //     if (output) {
        //         output += ', ';
        //     }
        //     output += `
        //     public ${v}: ${model}
        //     `
        // });
        // gulp.src('Must-No-Have-A-Real-Path/**.js')
        //     .pipe(file('model' + index + '.ts', output))
        //     .pipe(gulp.dest(configurations.path + '/'));

        done();

    };
    exports.interfacesCreator = interfacesCreator;
})();


