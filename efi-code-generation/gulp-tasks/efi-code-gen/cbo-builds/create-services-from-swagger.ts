(function () {
    // let counterefi = 0;
    const tree = require('./functions/tree-builder');
    const createServicesR = require('./functions/create-services-r');
    const saveFile = require('./functions/save-file');
    const createModule = require('./functions/create-module');
    

    function servicesCreator(json, done) {
        
        console.log("Creating services....................");
        let counter = 0;
        let treeBuilder = {};

        for (const path in json.paths) {
            counter++;
            const foldersPath = path.split("/").filter(String);
            treeBuilder = tree.addTreeR(foldersPath, treeBuilder, json.paths[path], path);
        }
        console.log(counter + " services created successfully");

        createServicesR(treeBuilder);

        saveFile("encoder.ts", encoder(), '/encoder');

        saveFile("cbo.module.ts", createModule(treeBuilder));

        done();

    };

    exports.servicesCreator = servicesCreator;

    function encoder() {
        return `/* tslint:disable */
        import { HttpUrlEncodingCodec } from '@angular/common/http';
        export class CustomHttpUrlEncodingCodec extends HttpUrlEncodingCodec {
            encodeKey(k: string): string {
                k = super.encodeKey(k);
                return k.split('+').join('%2B');
            }
            encodeValue(v: string): string {
                v = super.encodeValue(v);
                return v.split('+').join('%2B');
            }
        }
        `;
    }
})();
