

    module.exports = function createServicesR(treeBuilder) {
        for (const key in treeBuilder) {
            const tree = treeBuilder[key];
            if (Object.keys(tree.folder).length) {
                createServicesR(tree.folder);
            }
            tree.write();
        }
    }
