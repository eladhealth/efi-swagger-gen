

module.exports = {
    
    methodTemplate: (get) => {
    
        const template = `
        public {methodName}({parameters}): Observable<{returnType}> {
            {httpParams}
            {call}
        }\n\n
        `
        return template.replace("{methodName}", get.methodName)
        .replace("{parameters}", get.parameters)
        .replace("{returnType}", get.returnType)
        .replace("{httpParams}", get.httpParams)
        .replace("{call}", get.call);
    },


    classTemplate:`\n
    @Injectable()
    export class {name}ServiceApi {
        constructor(protected httpClient: ApplicationHttpClient) {
        }
        {output}
    }
    `,


    defaultImports: (stepBack: string) => {
        return `
        /* tslint:disable */
            import { Injectable } from '@angular/core';
            import { HttpParams } from '@angular/common/http';
            import { CustomHttpUrlEncodingCodec } from 'src/swagger-api-generator/encoder/encoder';
            import { environment } from 'src/environments/environment';
            import { Observable } from 'rxjs';
            import { ApplicationHttpClient } from '@appRoot/app-http-client';
            `
        },

    
        interfaces: (_import, stepBack) => {
        return `import { ${_import} } from 'src/swagger-api-generator/models/${_import}';\n`;
    }
}
    