(function () {

    const bindGets = require("./../functions/methods/bind-gets");
    const templates = require("./../helpers/templates");
    const saveFile = require("./../functions/save-file");


    module.exports = {

        methodFormat: (
            methodName: string,
            parameters: string,
            returnType: string,
            httpParams: string,
            call: string,
            imports: Array<string>,
            entirePath: string,
            counterefi: number
        ) => new MethodsFormat(methodName, parameters, returnType,
            httpParams, call, imports, entirePath, counterefi
        ),


        addTreeR: (
            folders: Array<string>, treeBuilder: {},
            methods: any, entirePath: string
        ) => addTreeR(folders, treeBuilder, methods, entirePath),


    }




    let counterefi = 0;

    class TreeBuilder {

        public folder = {};
        public methods: MethodsBuilder[] = [];
        public paths: string[] = [];

        constructor(public name: string) { }

        write() {

            const methods: any[] = [];
            this.methods.map(method => {
                this.paths.push(method.entirePath);
                methods.push(method.format(counterefi));
                counterefi++;
            });

            let output = "";
            const allImports = [];


            const duplicationChecker = {}; 32
            methods.map(method => {
                if (method.gets) {
                    method.gets.imports.map(imp => {
                        if (duplicationChecker[imp] || ['boolean', 'number', 'string'].indexOf(imp) !== -1) {
                            return;
                        }
                        duplicationChecker[imp] = 'imp';

                        if(allImports.indexOf(imp) === -1) {
                            allImports.push(imp);
                        }
                    });
                    output += templates.methodTemplate(method.gets)
                }
                if (method.posts) {
                    method.posts.imports.map(imp => {
                        if (duplicationChecker[imp] || ['boolean', 'number', 'string'].indexOf(imp) !== -1) {
                            return;
                        }
                        duplicationChecker[imp] = 'imp';

                        if(allImports.indexOf(imp) === -1) {
                            allImports.push(imp);
                        }
                    });
                    output += templates.methodTemplate(method.posts)
                }
            });

            let pa;
            let steps = '.';
            if (this.paths[0]) {
                const arr = this.paths[0].split("/");
                do {
                    pa = arr.pop();
                } while (pa.indexOf("{") !== -1);
                arr.forEach((_, index) => {
                    if(index !== 0) {
                        steps += '/..';
                    }
                });
            }


            let interfaces = '';
            allImports.forEach(_import => {
                if (_import !== 'any')
                interfaces += `
                import { ${_import} } from 'src/swagger-api-generator/models/${_import}';
                `
            });
            allImports.length === 0 ? '' : 
                templates.interfaces(allImports.join(", "), steps) ;

                
            const service = templates.defaultImports(steps) +
                interfaces + templates.classTemplate.replace("{name}", this.name)
                    .replace("{output}", output)


            const method = this.methods[0];
            //////////////////////////////////////////////////
            if (method) {
                let pa;
                const arr = method.entirePath.split("/");
                do {
                    pa = arr.pop();
                } while (pa.indexOf("{") !== -1);
                pa = arr.pop();
                const name = this.name + "ServiceApi";
                if(name !== 'apiServiceApi') {
                    saveFile(name + ".ts", service, arr.join("/") + "/" + pa);
                }
            }
        }
    }




    class MethodsBuilder {
        constructor(
            public name: string,
            public methods: any,
            public entirePath: string
        ) { }

        format(counterefi): any {



            let gets;
            if (this.methods.get) {
                gets = bindGets(this.methods.get, this.entirePath, 'Get', counterefi)
            }
            let posts;
            if (this.methods.post) {
                posts = bindGets(this.methods.post, this.entirePath, 'Post', counterefi)
            }
            // const posts = bindGets(this.methods.pots)

            return {
                gets, posts
            };
        }
    }

    class MethodsFormat {
        constructor(
            public methodName: string,
            public parameters: string,
            public returnType: string,
            public httpParams: string,
            public call: string,
            public imports: Array<string>,
            public entirePath: string,
            public counterefi
        ) { }
    }

    function addTreeR(
        folders: Array<string>,
        treeBuilder: {},
        methods: any,
        entirePath: string
    ): {} {
        const name = folders.shift();
        if (!treeBuilder[name]) {
            treeBuilder[name] = new TreeBuilder(name);
        }
        if (folders.length === 1) {
            treeBuilder[name].methods.push(
                new MethodsBuilder(folders.shift(), methods, entirePath)
            );
            return treeBuilder;
        }
        (treeBuilder[name] as TreeBuilder).folder = addTreeR(
            folders,
            treeBuilder[name].folder,
            methods,
            entirePath
        );
        return treeBuilder;
    }

})();
