(function () {

    const bindGets = require("./../functions/methods/bind-gets");


    module.exports = (
        methodName: string,
        parameters: string,
        returnType: string,
        httpParams: string,
        call: string,
        imports: Array<string>,
        entirePath: string,
        counterefi: number
    ) => new MethodsFormat(methodName, parameters, returnType,
        httpParams, call, imports, entirePath, counterefi
    )

    class MethodsFormat {
        constructor(
            public methodName: string,
            public parameters: string,
            public returnType: string,
            public httpParams: string,
            public call: string,
            public imports: Array<string>,
            public entirePath: string,
            public counterefi
        ) { }
    }


})();
