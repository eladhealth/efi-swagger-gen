




(function () {

    module.exports = function removeCharsR(val: string, arr: string[] = ['+','.', '_']) {
        arr.forEach(elem => {
            val = val.split(elem).join('');
        });
        for (let i = 0; i < arr.length; i++) {
            if (val.indexOf(arr[i]) !== -1) {
                return removeCharsR(val, arr);
            }
        }
        return val;
    };

})();



