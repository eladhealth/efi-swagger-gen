



(function () {

    const removeCharsR = require('./chars');

    module.exports = function getType(property) {

        switch (property.type) {

            case 'array':
                const ref = property.items.$ref;
                if (!ref) {
                    return getType(property.items);
                }
                if (ref.indexOf('#/definitions/0') !== -1) {
                    return '';
                }
                const val = ref.substr(ref.lastIndexOf('/') + 1);
                return removeCharsR(val);

                case 'object':
                const ref2 = property.items || property.additionalProperties;
                if (ref2) {
                    return getType(ref2);
                }
                return '';

            case 'string':
            case 'number':
            case 'integer':
            case 'type':
            case 'boolean':
                return '';

        }

        if (property.$ref) {
            return removeCharsR(property.$ref.substr(property.$ref.lastIndexOf('/') + 1));
        }

        if (property.schema) {
            if (property.schema.type) {
                return getType(property.schema);
            }
            if (!property.schema.$ref) {
                return getType(property.schema);
            }
            return removeCharsR(property.schema.$ref.substr(property.schema.$ref.lastIndexOf('/') + 1));
        }

        if (Object.keys(property).length === 0) {
            return '';
        }
        console.log('UNKNOW VALUE: ' + JSON.stringify(property));
        return 'any /*UNKNOW VALUE: ' + JSON.stringify(property) + ' */' ;
    };

})();

