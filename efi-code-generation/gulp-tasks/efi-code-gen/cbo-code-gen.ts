(function () {

    const createInterfacesFromSwagger = require('./cbo-builds/create-interfaces-from-swagger');
    const createServicesFromSwagger = require('./cbo-builds/create-services-from-swagger');
    const req = require('request');
    const {series} = require("gulp");
    const configurations = require('./cbo-builds/helpers/configurations')

    function cbocodegen(done) {
        console.log('1111');
        req(configurations.swaggerURL, null, function (err,data) {
            // console.log(data);
            
            const json = JSON.parse(data.body);
            for (let i = 0; i < 10; i++) { console.log('.'); }
            const interfaces = createInterfacesFromSwagger.interfacesCreator(json, () => true);
            const services = createServicesFromSwagger.servicesCreator(json, () => true);
        });

        // return '\n\n\n\n\n' + GetDefaults(services) + '\n\n\n\n\n' +  interfaces;
        // return interfaces.join('\n');

        done();
    };
    exports.cbocodegen = series(cbocodegen);

})();

