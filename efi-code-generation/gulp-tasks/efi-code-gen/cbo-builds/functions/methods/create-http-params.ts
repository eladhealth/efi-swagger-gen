
(function () {

    module.exports = get => {

        if (!get.parameters || !get.parameters.length) {
          return "";
        }
    
        const http = [
          `let params = new HttpParams({ encoder: new CustomHttpUrlEncodingCodec() });\n`
        ];

        get.parameters.map(param => {
          const val = param.name.split('.').pop();
          http.push(`
            if(${val} !== undefined) {
                params = params.set('${val}', <any>${val});
            }\n`);
        });
        return http.join("\n");
      }

})()
