



(function() {


    const getType = require('./types');
    const isTypeToImport = require('./type-to-import');

    module.exports = (parameters: Array<any>) => {
        const query = [];
        if (!parameters) {
            return {
                imports: [],
                content: ''
            };
        }
        const __imports = [];
        parameters.map(param => {
            const val = param.name.split('.').pop();
            const _import = isTypeToImport(param);
            if (_import) {
                __imports.push(
                    _import
                );
            }
            query.push(val + ': ' + getType(param));
        });
        return {
            imports: __imports,
            content: query.join(', ')
        };
    };


})();


