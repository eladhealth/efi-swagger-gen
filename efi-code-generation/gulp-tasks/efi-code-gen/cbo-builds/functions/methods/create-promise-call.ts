

(function () {


    const removeCharsR = require('../../helpers/chars');
    const getReturnType = require('../../helpers/return-types');

    module.exports = (get, path, params, method) => {

        const response = removeCharsR(getReturnType(get.responses));
        params = params.content.length ? (method === "Get" ? "{ params }" : " request ") : "";
        let _import = response;
        if (_import.indexOf('>') !== -1) {
            _import = _import.split('<').pop().split('>')[0];
        }
        const __imports = [];
        if (('string-void').indexOf(_import) === -1) {
            if (_import) {
                __imports.push(
                    _import
                );
            }
        }

        return ({
            imports: __imports,
            content: `
                return this.httpClient.${method}<${response}>(\`\${environment.baseUrl}${path}\`,
                ` + params + `);`
        });
    }


})()
