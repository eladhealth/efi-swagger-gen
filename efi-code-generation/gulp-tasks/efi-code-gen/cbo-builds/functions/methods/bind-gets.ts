
(function () {

    const methodFormat = require('./../method-format');
    const buildParams = require('./../../helpers/build-params');
    const getType = require('./../../helpers/types');
    const CreateHttpParams = require('./create-http-params');
    const CreateMethodHeader = require('./create-method-header');
    const CreatePromiseCall = require('./create-promise-call');

    module.exports = (method, entirePath, get, counterefi) => {
        const methodName = method.operationId.substring(method.tags[0].length + 1); // for next generation generated api
        const params = buildParams(method.parameters);

        const methodType = Object.keys(method.responses)[0];
        const returnType = getType(method.responses[methodType]);
        const httpParams = CreateHttpParams(method);
        const promiseCall = CreatePromiseCall(
            method,
            entirePath,
            params,
            get
        );
        const call = promiseCall.content;
        const _imports = [...params.imports, ...promiseCall.imports];


        const parameters = params.content;
        
        return methodFormat(
            methodName,
            parameters,
            returnType,
            (get === 'Post' ? '' : httpParams),
            call,
            _imports,
            this.entirePath,
            counterefi
        );
    }
    
})();
